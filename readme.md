# git cheat sheet

Read your group instruction in the text files

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)


[group4.md](group4.md)

and place your solution in your corresponding text file. Later, we will integrate all solutions into this file.

## Creating repositories

    $ git init                      Erzeugt ein git-Repo
    $ git clone https://...         Kopiert ein Repo in das aktuelle lokale Verzeichnis

## Staging and committing

    $ git add test.txt              Staged die Datei test.txt
    $ git reset HEAD test.txt       Unstaged die vorher gestagede Datei test.txt
    $ git add .                     Staged alle Änderungen und neue Dateien
    $ git add -u                    Staged alle Änderungen und Löschaktionen nur von getrackten Dateien
    $ git add -A                    Staged beides (. und -u Befehle)
    $ git diff --staged             Zeigt Differenzen der getrackten zu gestageden Änderungen
    $ git commit                    Gestagede Änderungen werden ins Repo übertragen
    $ git commit -a                 Staged und committed alle getrackten Dateien
    $ git commit -m "My message"    Commited und fügt Commit Description hinzu

## Inspecting the repository and history


    $ git diff                                      Shows differences between two branches or files or other instances and needs a reference)
    $ git diff --staged                             Shows differences between staged file and last commit of file in HEAD
    $ git diff test.txt                             Shows differences between unstaged file and last commit of file in HEAD
    $ git diff --theirs                             Shows differeces between the working tree and "their" branch (split branch with other users working on a project)
    $ git diff --ours                               Shows differences between the working tree and our branch
    $ git log                                       Shows the history of commits in repo and where the current branch label is attached
    $ git log --oneline                             Shows summarized version of git log (current HEAD branch)
    $ git log --oneline --all                       Shows summarized version of git log (all branches)
    $ git log --oneline --all -- graph              Shows summarized, visualized versio of git log (all branches)
    $ git log --oneline --all -- graph -- decorate  Shows summarized, visualized versio of git log (all branches) (is executed automatically and shows the HEAD pointer)
    $ git log --follow -p -- filename               Shows complete history of all parents and commits of a file (including renames etc.)
    $ git log -S'static void Main'                  Shows the log for the -S(searched) term
    $ git log --pretty=format:"%h - %an, %ar : %s"  Shows a log in an own built format. Parameters: %an= author, %ar= time since, %h = abbreviated commit hash, %s = command line by which the commit was reached

## Managing branches

    $ git checkout master      Verschiebt den HEAD-Zeiger auf den letzten commit des Masterbranches (Wechsel auf den Masterbranch).
    $ git branch feature1      Erzeugt einen neues Label mit dem Namen "feature1" (neuer Branch).
    $ git checkout -b work     Erzeugt eine neues Label mit dem Namen "work" und verschiebt den HEAD-Zeiger auf diesen (Wechsel auf den Branch "work").
    $ git checkout work        Verschiebt den HEAD-Zeiger auf den letzten commit von dem Branch "work"
    $ git branch               Zeigt alle Branchlabels an.
    $ git branch -v            Zeigt alle Branchlabels mit dem jeweils letzten commit an und markiert den aktuellen HEAD-Branch.
    $ git branch -a            Zeigt alle Branchlabels an, auch remotes und markiert den aktuellen HEAD-Branch.
    $ git branch --merged      Zeigt alle Branchlabels an, die in sich in der Historie vom HEAD befinden.
    $ git branch --no-merged   Zeigt alle Branchlabels an, die in sich nicht in der Historie vom HEAD befinden.
    $ git branch -d work       Löscht das Branchlabel "work", solange keine commits in Vergessenheit geraten (nicht mehr Teil der offiziellen Geschichte).
    $ git branch -D work       Löscht das Branchlabel "work" unabhängig vom commit-Status

## Merging

$ git checkout work                        Updates the working directory to the branch "work"
    $ git checkout master                    Updates the working directory to branch "master"
    $ git merge work                        Commits changes from the working directory to the master
    $ git merge --abort                        Reverses a merge
    $ git cat-file -p a3798b                Prints the content of the git object corresponding to the hash a3798b



Example 1:

    A - B (master)
         \
          C (work)

    $ git checkout master
    $ git merge work

    A - B
         \
          C (work), (master)

Result and explanation here:    1) Moves working directory to the master branch and 2) merges the changes done in "work" to "master"


Example 2:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout master
    $ git merge work

    A - B - C - D - E (master)
         \           /
          X ----- Y (work)

Result and explanation here:    1) Moves working directory to the master branch and 2) merges the changes done in "work" to "master";
                                    possibly merge conflicts to resolve



Example 3:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

    A - B - C - D - E (master), (work)
         \           /
          X ----- Y

Result and explanation here:    1) Moves working directory to the "work" branch, 2) brings changes from "master" to "work" (merge conflicts might need to
                                    be resolved), 3) moves working directory to the "work" branch and 4) merges the changes done in "work" to the
                                    "master" branch (now there should be no merge conflicts), i.e. a fast-forward