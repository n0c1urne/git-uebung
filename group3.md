Exercise:

Explain the following commands with one sentence:

    $ git diff
    $ git diff --staged
    $ git diff test.txt
    $ git diff --theirs
    $ git diff --ours
    $ git log
    $ git log --oneline
    $ git log --oneline --all
    $ git log --oneline --all --graph
    $ git log --oneline --all --graph --decorate
    $ git log --follow -p -- filename
    $ git log -S'static void Main'
    $ git log --pretty=format:"%h - %an, %ar : %s"

Answers:

    $ git diff         Shows differences between two branches or files or other instances and needs a reference)
	$ git diff --staged Shows differences between staged file and last commit of file in HEAD
	$ git diff test.txt Shows differences between unstaged file and last commit of file in HEAD
    $ git diff --theirs Shows differeces between the working tree and "their" branch (split branch with other users working on a project)
	$ git diff --ours   Shows differences between the working tree and our branch
	$ git log 			Shows the history of commits in repo and where the current branch label is attached
	$ git log --oneline Shows summarized version of git log (current HEAD branch)
	$ git log --oneline --all Shows summarized version of git log (all branches)
	$ git log --oneline --all -- graph Shows summarized, visualized versio of git log (all branches)
	$ git log --oneline --all -- graph -- decorate Shows summarized, visualized versio of git log (all branches) (is executed automatically and shows the HEAD pointer)
    $ git log --follow -p -- filename Shows complete history of all parents and commits of a file (including renames etc.)
    $ git log -S'static void Main'  Shows the log for the -S(searched) term
    $ git log --pretty=format:"%h - %an, %ar : %s"	 Shows a log in an own built format. Parameters: %an= author, %ar= time since, %h = abbreviated commit hash, %s = command line by which the commit was reached