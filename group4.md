Exercise:

Explain the following commands with one sentence:

    $ git checkout work                        Updates the working directory to the branch "work"
    $ git checkout master                    Updates the working directory to branch "master"
    $ git merge work                        Commits changes from the working directory to the master
    $ git merge --abort                        Reverses a merge
    $ git cat-file -p a3798b                Prints the content of the git object corresponding to the hash a3798b



Example 1:

    A - B (master)
         \
          C (work)

    $ git checkout master
    $ git merge work

    A - B
         \
          C (work), (master)

Result and explanation here:    1) Moves working directory to the master branch and 2) merges the changes done in "work" to "master"


Example 2:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout master
    $ git merge work

    A - B - C - D - E (master)
         \           /
          X ----- Y (work)

Result and explanation here:    1) Moves working directory to the master branch and 2) merges the changes done in "work" to "master";
                                    possibly merge conflicts to resolve



Example 3:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

    A - B - C - D - E (master), (work)
         \           /
          X ----- Y

Result and explanation here:    1) Moves working directory to the "work" branch, 2) brings changes from "master" to "work" (merge conflicts might need to
                                    be resolved), 3) moves working directory to the "work" branch and 4) merges the changes done in "work" to the
                                    "master" branch (now there should be no merge conflicts), i.e. a fast-forward
