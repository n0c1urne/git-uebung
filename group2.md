Exercise:

Explain the following commands with one sentence:

    $ git checkout master      Verschiebt den HEAD-Zeiger auf den letzten commit des Masterbranches (Wechsel auf den Masterbranch).
    $ git branch feature1      Erzeugt einen neues Label mit dem Namen "feature1" (neuer Branch).
    $ git checkout -b work     Erzeugt eine neues Label mit dem Namen "work" und verschiebt den HEAD-Zeiger auf diesen (Wechsel auf den Branch "work").
    $ git checkout work        Verschiebt den HEAD-Zeiger auf den letzten commit von dem Branch "work"
    $ git branch               Zeigt alle Branchlabels an.
    $ git branch -v            Zeigt alle Branchlabels mit dem jeweils letzten commit an und markiert den aktuellen HEAD-Branch.
    $ git branch -a            Zeigt alle Branchlabels an, auch remotes und markiert den aktuellen HEAD-Branch.
    $ git branch --merged      Zeigt alle Branchlabels an, die in sich in der Historie vom HEAD befinden.
    $ git branch --no-merged   Zeigt alle Branchlabels an, die in sich nicht in der Historie vom HEAD befinden.
    $ git branch -d work       Löscht das Branchlabel "work", solange keine commits in Vergessenheit geraten (nicht mehr Teil der offiziellen Geschichte).
    $ git branch -D work       Löscht das Branchlabel "work" unabhängig vom commit-Status

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.