Exercise:

Explain the following commands with one sentence:

    $ git init                      Erzeugt ein git-Repo
    $ git clone https://...         Kopiert ein Repo in das aktuelle lokale Verzeichnis
    $ git add test.txt              Staged die Datei test.txt
    $ git reset HEAD test.txt       Unstaged die vorher gestagede Datei test.txt
    $ git add .                     Staged alle Änderungen und neue Dateien
    $ git add -u                    Staged alle Änderungen und Löschaktionen nur von getrackten Dateien
    $ git add -A                    Staged beides (. und -u Befehle)
    $ git diff --staged             Zeigt Differenzen der getrackten zu gestageden Änderungen
    $ git commit                    Gestagede Änderungen werden ins Repo übertragen
    $ git commit -a                 Staged und committed alle getrackten Dateien
    $ git commit -m "My message"    Commited und fügt Commit Description hinzu

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.